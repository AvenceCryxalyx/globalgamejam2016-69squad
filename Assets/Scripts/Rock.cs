﻿using UnityEngine;
using System.Collections;

public class Rock : MonoBehaviour
{
    float timeElapsed;
    public float LifeSpan;

    void Update()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= LifeSpan)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (GetComponent<CircleCollider2D>().IsTouchingLayers(LayerMask.GetMask("Default", "Surface")))
        {
            Destroy(this.gameObject);
        }
    }
}
