﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    // Use this for initialization
    float timeElapsed;
    public float LifeSpan;
    public GameObject Bullet;
    //Rigidbody2D rb;

    void Start()
    {
        //rb.GetComponent<Rigidbody2D>();
    }

    public void Shoot(Vector2 dir, float speed)
    {
        GameObject bullet;
        bullet = Instantiate(Bullet, transform.position, Quaternion.identity) as GameObject;
        bullet.GetComponent<Rigidbody2D>().AddForce(dir.normalized * speed);
        bullet.GetComponent<Rock>().LifeSpan = LifeSpan;
    }
}
