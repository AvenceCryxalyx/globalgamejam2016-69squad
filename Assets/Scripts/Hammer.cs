﻿using UnityEngine;
using System.Collections;

public class Hammer : MonoBehaviour
{
    float timeElapsed;
    public float LifeSpan;
    public Rigidbody2D rb;

    void awake() 
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update() 
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= LifeSpan) 
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Wall"))
        {
            Destroy(other.gameObject);
        }
    }
}
