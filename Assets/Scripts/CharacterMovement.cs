﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour
{
    public float MovementSpeed;
    public float JumpForce;
    public float TopSpeed;
    Rigidbody2D rb;

    bool grounded;
    public bool flip = false;
    Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        grounded = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        grounded = GetComponent<BoxCollider2D>().IsTouchingLayers(LayerMask.GetMask("Surface"));
        if (Input.GetKey(KeyCode.A))
        {
            anim.Play("SwordsmanWalk");
            rb.AddForce(new Vector2(-MovementSpeed * 1000f, 0.0f) * Time.deltaTime);
            if (flip)
            {
                transform.Rotate(0, 180, 0);
                flip = false;
            }
        }

        else if (Input.GetKey(KeyCode.D))
        {
            anim.Play("SwordsmanWalk");
            rb.AddForce(new Vector2(MovementSpeed * 1000f, 0.0f) * Time.deltaTime);
            if (!flip)
            {
                transform.Rotate(0, 180, 0);
                flip = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            rb.AddForce(new Vector2(0.0f, JumpForce * 10000f) * Time.deltaTime);
        }

        Vector2 tepsped = rb.velocity;
        tepsped.x = Mathf.Clamp(rb.velocity.x, -TopSpeed, TopSpeed);
        if (tepsped.magnitude >= TopSpeed) 
        {
            rb.velocity = tepsped;
        }

    }
}
