﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public int HP;
    public float atkspd;
    float timeElapsed;
    public bool atk;

    // Use this for initialization
    void Start()
    {
        GetComponent<Attack>().enabled = false;
        GetComponent<Attack>().HitBox.enabled = false;
        timeElapsed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (HP <= 0) 
        {
            Destroy(gameObject);
        }

        timeElapsed += Time.deltaTime;

        if (timeElapsed >= atkspd && atk)
        {
            GetComponent<Attack>().enabled = true;
            GetComponent<Attack>().HitBox.enabled = true;
            timeElapsed = 0;
        }

    }
}
