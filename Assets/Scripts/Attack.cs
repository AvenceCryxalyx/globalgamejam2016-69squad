﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour
{
    float timeElapsed;
    public BoxCollider2D HitBox;

    void Update() 
    {
        if (HitBox.enabled)
        {
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= 0.5f) 
            {
                HitBox.enabled = false;
                this.enabled = false;
                timeElapsed = 0;

            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy") && this.gameObject.tag == "TopKek" && enabled)
        {
            other.GetComponent<Enemy>().HP--;
            Debug.Log("w3w");
        }

        else if (other.CompareTag("Player") && this.gameObject.tag == "Enemy" && enabled)
        {
            Destroy(other.gameObject);
            Debug.Log("WEW");
        }
    }
}
