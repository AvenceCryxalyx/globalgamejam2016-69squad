﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour 
{
	public GameObject Player;
	public float Smoothing;
	Vector3 Velocity;
	Vector3 offset;
	Vector3 targetPosition;

	// Use this for initialization
	void Start () 
	{
		Velocity = Vector3.zero;
		offset = transform.position - Player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		targetPosition = Player.transform.position + offset;
		transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref Velocity, Smoothing); 
	}
}
