﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
    List<GameObject> Pickups = new List<GameObject>();
    public Vector3 Offset;
    public GameObject Hammer;
    public float atkspd;

    public float RockCount;
    float HammerCount;

    float timeElapsed;
    
    void Start()
    {
        GetComponent<Attack>().HitBox.enabled = false;
        GetComponent<Attack>().enabled = false;
        RockCount = 0;
        HammerCount = 0;
    }


    void Update()
    {
        timeElapsed += Time.deltaTime;

        if (GetComponent<Attack>().enabled)
        {
            this.gameObject.tag = "TopKek";
        }
        else 
        {
            this.gameObject.tag = "Player";
        }
   
        if (atkspd >= timeElapsed)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                GetComponent<Attack>().enabled = true;
                GetComponent<Attack>().HitBox.enabled = true;
            }
            timeElapsed = 0;
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && RockCount > 0)
        {
            RockCount--;
        }

        if (Input.GetKeyDown(KeyCode.Mouse1) && HammerCount > 0)
        {
            useHammer();
            HammerCount--;
        }
    }

    void useHammer()
    {
        GameObject temp = Instantiate(Hammer, transform.position + Offset, Quaternion.identity) as GameObject;    
        if(!GetComponent<CharacterMovement>().flip)
        {
            temp.GetComponent<Hammer>().rb.AddForce(new Vector2(-200f, 50f));
        }
        else
        {
            temp.GetComponent<Hammer>().rb.AddForce(new Vector2(200f, 50f));
        }
        temp.GetComponent<Hammer>().LifeSpan = 0.3f;
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("HammerP"))
        {
            Destroy(other.gameObject);
            HammerCount++;
        }

        if (other.CompareTag("RockP"))
        {
            Destroy(other.gameObject);
            RockCount++;
        }
    }
}
