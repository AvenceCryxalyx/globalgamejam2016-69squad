﻿using UnityEngine;
using System.Collections;

public class AiChase : MonoBehaviour
{
    public int mindist = 5;
    public Transform target;
    public float moveSpeed = 3;
    public float range;
    bool ActChase;
    Vector3 distance;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        Vector3 targetDirection = target.position - transform.position;
        distance = transform.position - targetDirection;
        if (distance.magnitude <= range)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, moveSpeed);
        }
        Debug.Log(distance.magnitude);
    }
}
