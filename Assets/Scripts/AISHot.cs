﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class AISHot : MonoBehaviour
{
    public float speed;
    public float tilt;
    public Boundary boundary;
    public float Movespeed;

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    public float timer;

    Vector3 Target;
    Vector3 TargetDir;

    private float nextFire;
    void start()
    {
        Target = GameObject.FindGameObjectWithTag("Enemy").transform.position;
        TargetDir = Target - this.transform.position;
       
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Target = GameObject.FindGameObjectWithTag("Enemy").transform.position;
            TargetDir = Target - shot.transform.position;
            //nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            timer += 1.0F * Time.deltaTime;
            
            
        }
    }

    void FixedUpdate()
    {
        shot.transform.position += TargetDir.normalized * Movespeed * Time.deltaTime;
   
    }
}