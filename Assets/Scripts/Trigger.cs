﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour
{
    bool lever = false;
    GameObject updy;
    float moveSpeed = 3f;
    Vector3 stop = new Vector3(10.0f,5.0f,0.0f);
    float stoppos;
    float startTime;
    float speed = 0.3f;
    float journeyLength;
    Transform startMarker;
    Transform endMarker;

    // Use this for initialization
    void Start()
    {
        startTime = Time.time;
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                lever = true;
            }
        }

    }
    // Update is called once per frame
    void Update()
    {
        if (lever)
        {
            Activation();

        }
    }

    void Activation()
    {
        updy = GameObject.FindGameObjectWithTag("Ground");
        
        updy.transform.position = Vector3.MoveTowards(updy.transform.position, stop, 10f * Time.deltaTime);
    }
    
}
