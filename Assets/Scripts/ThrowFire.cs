﻿using UnityEngine;
using System.Collections;

public class ThrowFire : MonoBehaviour
{
    // Use this for initialization
    public float ShootingForce;

    Vector3 Target;
    void Start()
    {

    }

    // Update is called once per frame
    public void Update()
    {
        Vector3 Mouseposition = Input.mousePosition;
        Target = Camera.main.ScreenToWorldPoint(Mouseposition);
        Vector3 direction = Target - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        if (Input.GetKeyDown(KeyCode.Mouse0) && GetComponent<Inventory>().RockCount > 0)
        {
            GetComponent<Projectile>().Shoot(direction, ShootingForce);
        }
    }
}
